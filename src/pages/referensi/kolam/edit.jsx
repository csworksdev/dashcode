import React from "react";
import { useLocation } from "react-router-dom";

const Edit = (props) => {
  const location = useLocation();
  const { title, data } = location.state;
  return (
    <div>
      <div>{title}</div>
      <div>{data.name}</div>
    </div>
  );
};

export default Edit;
